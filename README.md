# Sample data analysis on a wellness dataset

In this repository, I have put two notebooks of work on a wellness dataset. I have done analysis on how lifestyle factors influence score on a test. This information is not published yet and is still confidential, so please do not share it with anyone.

## clustering lifestyle factors recommender
Here, I try to recommend lifestyle factors to people and cluster them in groups.

## ml and info analysis
In that notebook, I have shown mutual information and model-based feature influence in our datasets.
